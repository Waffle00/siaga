package com.example.gemastik.modules.viewmodel

import com.example.gemastik.base.BaseModule
import org.koin.core.module.Module
import org.koin.dsl.module

object ViewModelModule : BaseModule {
    override val modules: List<Module>
        get() = listOf(viewModelModule)

    private val viewModelModule = module {
    }


}