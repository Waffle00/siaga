package com.example.onboardingfix.modules.repository

import com.example.gemastik.data.AppRepository
import com.example.onboardingfix.data.LocalDataSource
import com.example.onboardingfix.data.RemoteDataSource
import com.example.gemastik.base.BaseModule
import org.koin.core.module.Module
import org.koin.dsl.module

object RepositoryModule : BaseModule {
    override val modules: List<Module>
        get() = listOf(
            dataRepoModules,
            remoteDataSourcesModule,
            localDataSourceModule
        )

    private val dataRepoModules = module {
        single { AppRepository(get(), get()) }
    }

    private val remoteDataSourcesModule = module {
        single { RemoteDataSource(get()) }
    }

    private val localDataSourceModule = module {
        single { LocalDataSource(get()) }
    }
}
