package com.example.gemastik.data.request

import com.google.gson.annotations.SerializedName

class ProfileRequest(
    @SerializedName("access_token")
    val access_token : String
)