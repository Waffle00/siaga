package com.example.gemastik.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.gemastik.data.Dao.*
import com.example.gemastik.data.entity.*

@Database(
    entities = [
        ProfileEntity::class
    ],
    version = 6,
    exportSchema = false
)
abstract class AppDataBase : RoomDatabase() {
    abstract fun profileDao(): ProfileDao
}